from robot import *

# Fais avancer le robot jusqu'au portail puis valide que le robot est au bon
# endroit grâce à valider()

# Astuce:
# - grobot.sauter(sens='avant') permet de sauter en avant
# - grobot.sauter(force=2) permet de sauter plus fort
# - on peut combiner les 2 en séparant par une virgules grobot.sauter(sens='avant', force=2)

grobot.avancer()
grobot.sauter()




valider()
