from robot import *

# Fais avancer le robot jusqu'au portail puis valide que le robot est au bon
# endroit grâce à valider()

# Astuce:
# - grobot.regarder(x=1,y=0) permet de regarder la case à côté
#    ' ' => Rien
#    '#' => Brique/sol
#    'g' => Le robot vert
#    'G' => Le portail vert
# - grobot.reculer(4) permet de reculer de 4 cases vers la gauche


# IMPORTANT: il faut utiliser un if car le portail peut être soit 4 cases à gauche soit 4 cases à droite.

grobot.avancer()




valider()
