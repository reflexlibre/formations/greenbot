from robot import *
#  ___ ___    ___   _____    _ 
#  | _ ) _ \  /_\ \ / / _ \  | |
#  | _ \   / / _ \ V / (_) | |_|
#  |___/_|_\/_/ \_\_/ \___/  (_)
                              
# Tu as finis le jeu :)
# A toi de créer la suite en éditant ce niveau pour le transformer

# Liste des fichiers à éditer
# .assets/levels/ton-prenom.greenbot : Permet de définir les informations du niveau et le plan du niveau
# .assets/empty_levels/11-ton-prenom.py : Le programme à afficher au début du niveau avec les conseils pour le résoudre
# .solutions/11-ton-prenom.py : La solution
#
# ASTUCE: Sous linux, utilise CTRL + h dans l'explorateur de fichier
# pour faire apparaitre les fichiers cachés qui commencent par un point
#
# Tu peux aussi éditer le fichier .objets.yaml pour ajouter tes propres tuiles :)

grobot.avancer(2)

valider()
