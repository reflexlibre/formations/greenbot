from robot import *

# Fais avancer le robot jusqu'au portail puis valide que le robot est au bon
# endroit grâce à valider()

# Astuce:
# - grobot.regarder(x=0,y=1) permet d'obtenir la valeur de la case au dessus du robot
# - grobot.avancer(x) permet de faire avancer le robot de x cases

distance = grobot.regarder(x=0,y=1)
grobot.avancer(distance + 1)




valider()
