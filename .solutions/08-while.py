from robot import *

# Fais avancer le robot jusqu'au portail puis valide que le robot est au bon
# endroit grâce à valider()

# IMPORTANT: il faut utiliser une boucle while car le portail est plus ou moins loin
while grobot.regarder(x=0,y=0) != "G":
    grobot.avancer()


valider()
