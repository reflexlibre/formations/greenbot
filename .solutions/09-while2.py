from robot import *

# Fais avancer le robot jusqu'au portail puis valide que le robot est au bon
# endroit grâce à valider()

# IMPORTANT: il faut utiliser des boucles while car le portail et le trou sont plus ou moins loin

# Etape 1: avancer jusqu'au trou (partie à corriger)
while grobot.regarder(x=1,y=-1) != ' ':
    grobot.avancer()

# Etape 2: sauter en avant
grobot.sauter(sens='avant')


# Etape 3: avancer jusqu'au portail (en s'inspirant du niveau précédent)
while grobot.regarder(x=0,y=0) != "G":
    grobot.avancer()



valider()
