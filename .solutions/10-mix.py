from robot import *

# Fais avancer le robot jusqu'au portail puis valide que le robot est au bon
# endroit grâce à valider()

# IMPORTANT: 
#  - le premier trou est à une distance variable
#  - le drapeau peut appraitre à 3 endroits distincts (relancer plusieurs fois pour savoir où ou regarder le plan du niveau dans la console)

grobot.avancer()

valider()
