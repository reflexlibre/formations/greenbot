from robot import *

# Fais avancer le robot jusqu'au portail puis valide que le robot est au bon
# endroit grâce à valider()

# IMPORTANT: il faut utiliser des boucles while car le portail est plus ou moins loin

brobot.sauter(sens='avant', force=2)
brobot.pause()
while brobot.regarder(1,-1) != ' ':
    brobot.avancer()
brobot.avancer()

while grobot.regarder(1,-1) != ' ':
    grobot.avancer()
grobot.sauter(sens='avant', force=2)
grobot.avancer(2)
brobot.sauter()
while brobot.regarder(0,0) != 'B':
    brobot.reculer()

valider()
