# greenbot

Green bot is an arcade game to learn python in french

## Description
Greenbot est un puzzle game sous forme de jeu d'arcade qui sert à apprendre à programmer (à utiliser en complément de cours théorique). 

La personne qui joue doit programmer à l'aide d'instructions basiques un ou plusieurs robots et réussir à les emener sur un portail de la bonne couleur. 

Le jeu est facilement hackable pour répéter l'usage de certains concepts.

## Installation
Ce jeu requiert pygame en plus de python3.
Il est conseiller d'utiliser l'IDE thonny et d'installer pygame via Outils > Gérer les paquets > pygame > Installer.
Il faut ensuite ouvrir le fichier 01-avancer.py dans Thonny et lancer le programme avec le triangle vert 

NB: ce jeu n'a été testé que sous linux.

## TODO et contribution
Vos Merge request ou tickets idées sont particulièrement bienvenues si vous avez une idée pour illustrer les variables ou les listes.

## Authors and acknowledgment
Code: ljf
Robot image:

## License
AGPLv3

