# Pourquoi ce programme est en français ?
# Il est destiné à être utilisé et éventuellement lu par des élèves de 15 ans.


# import the pygame module, so you can use it
import pygame
import pygame.locals
import pygame.mixer
import time
import atexit
import __main__
import random
import os
import yaml
import re
import sys
import shutil

# initialize the pygame module
pygame.init()
pygame.mixer.init()


os.environ['SDL_VIDEO_WINDOW_POS'] = "0,0"
FPS = 30
WIDTH = 950
HEIGHT = 600
HELP_HEIGHT = 200
SQUARE_SIZE = 32
COULEUR = {
    'bleu': (166, 217, 249),
    'noir': (0, 0, 0)
}
def charger_image(chemin_image):
    return pygame.image.load(chemin_image)

with open(r'.objets.yaml') as f:
    objets = yaml.safe_load(f)
#TRAVERSABLE= ' oRGB'
#UNTRAVERSABLE = '#♯?□■rgb'
TRAVERSABLE= ' '
UNTRAVERSABLE = ''
ROBOTS =''
PORTALS = ''

for key, objet in objets.items():
    objet['image'] = charger_image(".assets/images/" + objet['image'])
    if 'traversable' in objet['groups']:
        TRAVERSABLE += key
    else:
        UNTRAVERSABLE += key

    if 'robots' in objet['groups']:
        ROBOTS += key
    if 'portals' in objet['groups']:
        PORTALS += key

fond_img = charger_image(".assets/images/fond.png")

sons = {
    "death": pygame.mixer.Sound(".assets/music/Death Screams/Robot/sfx_deathscream_robot1.wav"),
    "portals": pygame.mixer.Sound(".assets/music/Movement/Portals and Transitions/sfx_movement_portal1.wav"),
}

def compter_ligne():
    nb_lines = 0
    with open(sys.argv[0], 'r') as file:
        lines = file.readlines()
        for line in lines:
            if line.strip() not in ["valider()", "from robot import *"] and line.strip() and line.strip()[0] != "#":
                print(line)
                nb_lines += 1
    return nb_lines


def copier_niveau_suivant():
    niveau_actuel = sys.argv[0]
    flag = False
    for file_name in sorted(os.listdir(".assets/empty_levels/")):
        if flag:
            break
        if sys.argv[0] == file_name:
            flag = True
    if file_name == niveau_actuel:
        pygame.quit()
        sys.exit()
    else:
        shutil.copy(".assets/empty_levels/" + file_name, "./")
        time.sleep(3)
        os.system("thonny " + file_name)

def valider():
    niveau = Niveau.get_instance()
    grobot.tomber()
    if not niveau.est_reussi():
        perdu()
    elif compter_ligne() > niveau.info["max"]:
        perdu(texte="TROP DE LIGNES !!!")
    else:
        gagne()
        copier_niveau_suivant()

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()

atexit.register(valider)



FramePerSec = pygame.time.Clock()

screen = pygame.display.set_mode((WIDTH, HEIGHT + HELP_HEIGHT))

def afficher_le_nouvel_ecran():
    pygame.display.flip()
    FramePerSec.tick(FPS)

def remplir(couleur):
    screen.fill(couleur)

def afficher_texte(texte):
    my_font = pygame.font.SysFont("arial", 72)

    # Créer un texte
    text_score = my_font.render(texte, False, (0, 0, 0))

    # Afficher ce texte à un endroit précis
    screen.blit(text_score, (250, 300))
    afficher_le_nouvel_ecran()

class TextRectException:
    def __init__(self, message = None):
        self.message = message
    def __str__(self):
        return self.message

def render_textrect(string, font, rect, text_color, background_color, justification=0):
    """Returns a surface containing the passed text string, reformatted
    to fit within the given rect, word-wrapping as necessary. The text
    will be anti-aliased.

    Takes the following arguments:

    string - the text you wish to render. \n begins a new line.
    font - a Font object
    rect - a rectstyle giving the size of the surface requested.
    text_color - a three-byte tuple of the rgb value of the
                 text color. ex (0, 0, 0) = BLACK
    background_color - a three-byte tuple of the rgb value of the surface.
    justification - 0 (default) left-justified
                    1 horizontally centered
                    2 right-justified

    Returns the following values:

    Success - a surface object with the text rendered onto it.
    Failure - raises a TextRectException if the text won't fit onto the surface.
    """

    import pygame

    final_lines = []

    requested_lines = string.splitlines()

    # Create a series of lines that will fit on the provided
    # rectangle.

    for requested_line in requested_lines:
        if font.size(requested_line)[0] > rect.width:
            words = requested_line.split(' ')
            # if any of our words are too long to fit, return.
            for word in words:
                if font.size(word)[0] >= rect.width:
                    raise TextRectException("The word " + word + " is too long to fit in the rect passed.")
            # Start a new line
            accumulated_line = ""
            for word in words:
                test_line = accumulated_line + word + " "
                # Build the line while the words fit.
                if font.size(test_line)[0] < rect.width:
                    accumulated_line = test_line
                else:
                    final_lines.append(accumulated_line)
                    accumulated_line = word + " "
            final_lines.append(accumulated_line)
        else:
            final_lines.append(requested_line)

    # Let's try to write the text out on the surface.

    surface = pygame.Surface(rect.size)
    surface.fill(background_color)

    accumulated_height = 0
    for line in final_lines:
        if accumulated_height + font.size(line)[1] >= rect.height:
            raise TextRectException("Once word-wrapped, the text string was too tall to fit in the rect.")
        if line != "":
            tempsurface = font.render(line, 1, text_color)
            if justification == 0:
                surface.blit(tempsurface, (0, accumulated_height))
            elif justification == 1:
                surface.blit(tempsurface, ((rect.width - tempsurface.get_width()) / 2, accumulated_height))
            elif justification == 2:
                surface.blit(tempsurface, (rect.width - tempsurface.get_width(), accumulated_height))
            else:
                raise TextRectException("Invalid justification argument: " + str(justification))
        accumulated_height += font.size(line)[1]

    return surface


def perdu(stop=False, texte="GAME OVER !!!"):
    for x in [4, -4, 4, -4] *2:
        grobot.rect.x += x
        grobot.vitesse = 0
        Niveau.get_instance().redessiner()
    afficher_texte(texte)
    pygame.mixer.Sound.play(sons['death'])
    if stop:
        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()

def gagne():
    pygame.mixer.Sound.play(sons['portals'])
    afficher_texte("GAGNÉ !!!")




class Objet(pygame.sprite.Sprite):

    @staticmethod
    def create(lettre, x, y):
        if lettre in "rgb":
            return Robot(lettre, x, y)
        else:
            return Objet(lettre, x, y)

    def __init__(self, lettre, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.lettre = lettre
        self.image = objets[lettre]['image'].convert()
        self.image.set_colorkey((255, 255, 255))
        self.rect = self.image.get_rect()
        self.rect.x, self.rect.y = x * SQUARE_SIZE, y * SQUARE_SIZE
        self.x, self.y = x, y
        niveau = Niveau.get_instance()
        niveau.groups['all'].add(self)
        for group in objets[lettre]['groups']:
            niveau.groups[group].add(self)


    def dessiner(self):
        screen.blit(self.image, (self.rect.x, self.rect.y))

    def est_en_collision(self, liste_objets, enlever=False):
        if not isinstance(liste_objets, pygame.sprite.Group):
            group = pygame.sprite.Group()
            group.add(liste_objets)
        else:
            group = liste_objets
        return pygame.sprite.spritecollide(self, group, enlever)



class Niveau:
    __instance = None
    paysage_default = """


         ooo
        oooooooooo
           ooooo      ooooo
                        ooooo






"""

    @staticmethod
    def get_instance():
        if Niveau.__instance is None:
            Niveau.__instance = Niveau()
        return Niveau.__instance

    def __init__(self):
        if not Niveau.__instance:
            Niveau.__instance = self
        nom_niveau = __main__.__file__.split('/')[-1].replace(".py", "")
        nom_niveau = re.sub(r'^\d+-', '', nom_niveau)

        yaml_content = ""
        self.paysage = []
        dash_flag = False
        with open(f"./.assets/levels/{nom_niveau}.greenbot", encoding='utf8') as f:
            for line in f:
                line = line.rstrip()
                if line.startswith('-' * 32):
                    dash_flag = True
                elif dash_flag:
                    self.paysage.append(line)
                else:
                    yaml_content += line + "\n"
        self.info = yaml.safe_load(yaml_content)

        if len(self.paysage) <= 18:
            self.paysage = self.paysage_default.split('\n')[0:-len(self.paysage)+1] + self.paysage + [self.paysage[-1]] * 4
        x = random.randint(1, 5)
        y = random.randint(1, 3)
        miror = random.choice([1,-1])
        a = random.randint(0, 5)
        i = 0
        pos_portal = random.randint(0,"\n".join(self.paysage).count("G")-1)
        i_portal = 0
        for i, line in enumerate(self.paysage):
            print(line)
            if len(line) == 0:
                pass
            elif line[-1] == "E":
                line = line[:-1] + line[-2] * (32-len(line))
            for char in "".join(objets.keys()):
                line = line.replace(char+"x", char*x).replace(char+"y", char*y)
                line = line.replace("X", str(x)).replace("Y", str(y))

            for j, char in enumerate(line):
                if line[j] == 'G':
                    if i_portal != pos_portal:
                        line = line[:j] + ' ' + line[j+1:]
                    i_portal += 1


            if 'mirror' in self.info:
                mirror = random.choice([1,-1])
                line = line[::mirror]
            self.paysage[i] = line
            i+=1

        pygame.display.set_caption(f"Green Robot : {self.info['title']}")
        self.groups= {}
        self.groups['all'] = pygame.sprite.Group()
        for lettre, objet in objets.items():
            for group in objet['groups']:
                if group not in self.groups:
                    self.groups[group] = pygame.sprite.Group()

        for y, ligne in enumerate(self.paysage):
            for x, lettre in enumerate(ligne):
                if lettre not in ' ':
                    Objet.create(lettre, x=x, y=y)
        remplir(COULEUR['bleu'])
        self.redessiner()


    def numero_perso(self):
        return sum([ord(c) for c in __file__.split('/')[-2]])

    def redessiner(self):
        #remplir(COULEUR['bleu'])

        screen.blit(fond_img, (0, HEIGHT))
        my_font = pygame.font.SysFont("arial", 20)
        my_rect = pygame.Rect((0, 0, WIDTH-80, HELP_HEIGHT-80))
        surface = render_textrect(self.info['description'], my_font, my_rect, (10, 10, 10), (154,171,139), 0)
        screen.blit(surface, (40, HEIGHT +40))
        my_rect = pygame.Rect((0, 0, 290, 22))
        surface = render_textrect(f"Nombre de lignes maximum : {self.info['max']}", my_font, my_rect, (10, 10, 10), COULEUR["bleu"], 0)
        screen.blit(surface, (WIDTH-280, 10))

        for element in self.groups['all']:
            element.dessiner()
        for element in self.groups['robots']:
            element.dessiner()
        afficher_le_nouvel_ecran()

    def est_reussi(self):
        paysage = ''.join(self.paysage)
        for robot in self.groups['robots']:
            if robot.lettre.upper() in paysage and str(robot.regarder()) != robot.lettre.upper():
                return False
        return True



class Robot(Objet):
    def __init__(self, lettre, x, y):
        super().__init__(lettre, x=x, y=y)
        self.vitesse = 0
        self.rect.y += 1
        globals()[lettre + "robot"] = self

    def regarder(self, x=0, y=0):
        niveau = Niveau.get_instance()
        for robot in niveau.groups['robots']:
            if self != robot and self.x + x == robot.x and self.y-y == robot.y:
                return robot.lettre
        try:
            valeur_case = niveau.paysage[self.y - y][self.x + x]
            try:
                return int(valeur_case)
            except:
                return valeur_case
        except:
            return ' '

    def avancer(self, repetition=1, sens=1):
        if repetition < 0:
            repetition = -repetition
            sens = -sens
        if str(self.regarder(sens, -1)) in TRAVERSABLE and str(self.regarder(0, -1)) in TRAVERSABLE:
            self.tomber()
        if repetition > 1:
            self.avancer(repetition - 1, sens)
        if str(self.regarder(sens, 0)) not in UNTRAVERSABLE:
            self.x += sens
            for i in range(0, 4):
                self.rect.x += 8 * sens
                Niveau.get_instance().redessiner()
        else:
            self.pause()
            perdu(True)

        self.tomber()

    def reculer(self, repetition=1):
        self.avancer(repetition, sens=-1)

    def pause(self):
        self.avancer(sens=0)

    def sauter(self, sens=0, force=1):
        niveau = Niveau.get_instance()
        if str(self.regarder(0, -1)) in TRAVERSABLE:
            self.tomber()
        if str(self.regarder(0, -1)) in TRAVERSABLE:
            perdu(True)
        if sens == 'avant':
            sens = 1
        elif sens == 'arrière':
            sens = -1
        elif abs(sens) > 1:
            sens = sens / abs(sens)
        if force > 2:
            force = 2
        for i in range(0, force):
            if str(self.regarder(0, 1)) in UNTRAVERSABLE or str(self.regarder(sens, 1)) in UNTRAVERSABLE:
                perdu(True)
            else:
                if str(self.regarder(sens, 0)) in UNTRAVERSABLE:
                    for y in [8, 8, 8, 8]:
                        self.rect.y -= y
                        Niveau.get_instance().redessiner()
                    for x in [8, 8, 8, 8]:
                        self.rect.x += x * sens
                        Niveau.get_instance().redessiner()
                else:
                    for y in [8, 8, 8, 8]:
                        self.rect.x += sens * 8
                        self.rect.y -= y
                        Niveau.get_instance().redessiner()
                self.y -= 1
                self.x += sens
                self.vitesse = sens

    def tomber(self):
        niveau = Niveau.get_instance()
        while str(self.regarder(0, -1)) in TRAVERSABLE:
            if str(self.regarder(self.vitesse, 0)) in UNTRAVERSABLE or str(self.regarder(self.vitesse, -1)) in UNTRAVERSABLE:
                self.vitesse = 0
            for i in range(0, 4):
                if self.vitesse != 0:
                    self.rect.x += 8 * self.vitesse
                self.rect.y += 8
                niveau.redessiner()
            if self.vitesse != 0:
                self.x += self.vitesse
            self.y += 1
            if self.rect.y > HEIGHT - SQUARE_SIZE:
                perdu(True)
        self.vitesse = 0

Niveau.get_instance()
time.sleep(1)

